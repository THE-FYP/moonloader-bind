package = "moonloader-bind"
version = "1.0.0-1"
source = {
   url = "git+https://gitlab.com/THE-FYP/moonloader-bind.git",
   tag = "v1.0.0"
}
description = {
   summary = "Events for common tasks",
   detailed = "",
   homepage = "https://gitlab.com/THE-FYP/moonloader-bind",
   license = "MIT"
}
dependencies = {
   "lua >= 5.1, < 5.4"
}
build = {
   type = "builtin",
   modules = {
      bind = "lua/bind.lua"
   }
}
